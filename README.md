# Typescript project starter

Following project contains starter and config for simple typescript project. Default address: ```localhost:4100```

## Features

* Typescript
* Webpack
* Live dev server
* Karma & Jasmine
* TravisCI

## Installation

Open terminal and run next commands

```sh
npm i
npm run webpack:watch
```

## Testing

Open terminal and run next commands

```sh
npm i
npm run test
```